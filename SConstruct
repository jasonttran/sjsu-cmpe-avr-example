import os

from fsops import scan_tree, filter_files, ch_filename_ext
from helper import is_windows, is_linux
from osops import prepend_env_path

__author__ = "jtran"
__version__ = "1.0.0"


"""
Define naming conventions
"""
TARGET_NAME = Dir(".").name


"""
Define file nodes and directory nodes
"""
PROJECT_DIR = Dir(".")
VARIANT_DIR = Dir("build")
OBJ_DIR = VARIANT_DIR.Dir("obj")

INCLUDE_DIRS = [
    Dir("source"),
]

INCLUDE_DIRS_ROOT = [
    Dir("source/os"),
]

SRC_DIRS = [
    Dir("source"),
]

LINKER_FILES = []

EXCLUDED_SRC_FILES = []


"""
Define build environments
"""
avr_env = Environment(
    ENV=os.environ,
    tools=["mingw"],
    CC="avr-gcc",
    CXX="avr-g++",
    LD="avr-ld",
    AS="avr-as",
    OBJCOPY="avr-objcopy",
    OBJDUMP="avr-objdump",
    SIZE="avr-size",
    CCFLAGS=[
        "-O0",
        "-Wall",
        "-std=gnu99",
        "-mmcu=atmega328"],
    LINKFLAGS=["-mmcu=atmega328"],
    CPPPATH=[],
)


def objdump_generator(source, target, env, for_signature):
    return "$OBJDUMP --source --all-headers --demangle --line-numbers --wide $SOURCE > $TARGET"


avr_env["BUILDERS"]["Objcopy"] = Builder(action="$OBJCOPY -j .text -j .data -O ihex $SOURCE $TARGET", suffix=".hex", src_suffix=".elf")
avr_env["BUILDERS"]["Objdump"] = Builder(generator=objdump_generator)
avr_env["BUILDERS"]["Size"] = Builder(action="$SIZE --format=berkeley $SOURCE", src_suffix=".elf")

avr_env.VariantDir(variant_dir=VARIANT_DIR.name, src_dir=SRC_DIRS, duplicate=0)


"""
Search and group build files
"""

""" Search and group source files and source directories """
target_src_filenodes = []
target_src_dirnodes = []

for dirnode in SRC_DIRS:
    src_filenodes, src_dirnodes, _ = scan_tree(dirnode)
    target_src_filenodes.extend(src_filenodes)
    target_src_dirnodes.extend(src_dirnodes)


""" Search and group linker scripts """
for linker_file in LINKER_FILES:
    avr_env["LINKFLAGS"].append("-T{}".format(File(linker_file).abspath))

""" Search and group include paths """
avr_env["CPPPATH"].extend(INCLUDE_DIRS)

for dirnode in INCLUDE_DIRS_ROOT:
    _, _, include_dirnodes = scan_tree(dirnode)
    avr_env["CPPPATH"].extend(include_dirnodes)

""" Filter build files """
target_src_filenodes = filter_files(target_src_filenodes, EXCLUDED_SRC_FILES)


"""
Perform builds
"""
obj_filenodes = []
for src_filenode in target_src_filenodes:
    new_filename = ch_filename_ext(src_filenode, "o")
    dest_filepath = OBJ_DIR.File(new_filename.name)
    new_obj_filenodes = avr_env.Object(target=dest_filepath, source=src_filenode)
    obj_filenodes.extend(new_obj_filenodes)

elf_filenodes = avr_env.Program(target=VARIANT_DIR.File("{}.elf".format(TARGET_NAME)), source=obj_filenodes)
bin_filenodes = avr_env.Objcopy(target=VARIANT_DIR.File("{}.bin".format(TARGET_NAME)), source=elf_filenodes)
hex_filenodes = avr_env.Objcopy(target=VARIANT_DIR.File("{}.hex".format(TARGET_NAME)), source=elf_filenodes)
lst_filenodes = avr_env.Objdump(target=VARIANT_DIR.File("{}.lst".format(TARGET_NAME)), source=elf_filenodes)
avr_env.Size(elf_filenodes)

Depends(elf_filenodes, LINKER_FILES)

from fnmatch import fnmatch
import os
import sys

__author__ = "jtran"
__version__ = "1.0.0"


def is_windows():
	return "win" in sys.platform


def is_linux():
	return "linux" in sys.platform

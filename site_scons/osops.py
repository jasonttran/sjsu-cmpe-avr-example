"""
osops - OS related operations
"""

import os

from SCons.Script import *

__author__ = "jtran"
__version__ = "1.0.0"


def prepend_env_path(env, dirnode):
    """
    Prepend a directory to an Environment's PATH environment variable
    :param env: Environment object (Environment)
    :param dirnode: A directory node to prepend to the PATH environment variable (Dir)
    """
    env_vars = os.environ
    path = env_vars["PATH"]
    new_path = os.pathsep.join([Dir(dirnode).abspath, path])
    env_vars["PATH"] = new_path
    env["ENV"] = env_vars

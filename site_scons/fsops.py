"""
fsops - file system operations - SCons file nodes and directory nodes related operations
"""

import fnmatch
import glob
import os

from SCons.Script import *

__author__ = "jtran"
__version__ = "1.0.0"

DEFAULT_SRC_PATTERNS = ["*.c", "*.cpp"]
DEFAULT_INCLUDE_PATTERNS = ["*.h", "*.hpp"]


def scan_tree(dirnode, src_patterns=DEFAULT_SRC_PATTERNS, header_patterns=DEFAULT_INCLUDE_PATTERNS):
    """
    Recursively search/glob source files, header files, etc.
    :param dirnode: A root directory node - root of search tree (Dir)
    :param scr_patterns: A list of source file name patterns to search (list of str)
    :param header_patterns: A list of header file name patterns to search (list of str)
    :return: Tuple(
        A list of source file nodes (list of File),
        A list of directory nodes that contain a source file (list of Dir),
        A list of directory nodes that contain a header file (list of Dir),
    )

    Example usage:
    src_filenodes, src_dirnodes, include_dirnodes = scan_tree(Dir("L1_FreeRTOS"))
    """
    src_filenodes = []
    src_dirnodes = []
    include_dirnodes = []
    
    for dirpath, dirnames, filenames in os.walk(os.path.relpath(dirnode.abspath)):
        for src_pattern in src_patterns:
            matching_src_files = Glob(os.path.join(dirpath, src_pattern))
            src_filenodes.extend(matching_src_files)
            if Dir(dirpath) not in src_dirnodes:
                src_dirnodes.append(Dir(dirpath))
        for header_pattern in header_patterns:
            if (len(fnmatch.filter(filenames, header_pattern)) > 0) and (Dir(dirpath) not in include_dirnodes):
                include_dirnodes.append(Dir(dirpath))
    return (src_filenodes, src_dirnodes, include_dirnodes)


def filter_files(filenodes, exclude_filenodes=None, exclude_filename_pattern=None):
    """
    Filter file nodes
    :param filenodes: A list of file nodes (list of File)
    :param exclude_filenodes: A list of file nodes to filter out (list of File)
    :param exclude_filename_pattern: A file name pattern to filter out files with a matching file name pattern (str)
    :return: A list of filtered file nodes (list of File)
    """
    filenodes = map(File, filenodes)
    exclude_filenodes = map(File, exclude_filenodes)

    if exclude_filenodes is None:
        exclude_filenodes = []
    
    filtered_filenodes = []
    filtered_filenodes.extend(filter(lambda filenode: filenode not in exclude_filenodes, filenodes))

    if exclude_filename_pattern is not None:
        filtered_filenodes.extend(filter(lambda filenode: not fnmatch(filenode.name, exclude_filename_pattern), filtered_filenodes))

    return filtered_filenodes


def ch_filename_ext(filenode, ext):
    """
    Create a new File object with a modified file name extension
    :param filenode: A file node (File)
    :param ext: A new extension - i.e. "cpp" (str)
    :return: A file node with a modified file name extension (File)
    """
    filenode = File(filenode)
    basename, curr_ext = os.path.splitext(filenode.name)
    new_filename = "{}.{}".format(basename, ext)
    return File(os.path.join(os.path.dirname(filenode.abspath), new_filename))

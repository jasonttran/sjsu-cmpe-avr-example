#ifndef WDT_H
#define WDT_H


void WDT_init(void);
void WDT_reset(void);
void WDT_system_reset(void);


#endif  // WDT_H

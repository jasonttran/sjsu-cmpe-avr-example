#ifndef I2CSTATES_H
#define I2CSTATES_H


typedef enum {
    // Common
    START_SENT=0x08,
    REAP_START_SENT=0x10,
    ARBITRATION=0x38,

    // Master Transmitter Mode
    SLA_W_SENT_ACK_REC=0x18,
    SLA_W_SENT_NACK_REC=0x20,
    DAT_SENT_ACK_REC=0x28,
    DAT_SENT_NACK_REC=0x30,

    // Master Receiever Mode
    SLA_R_SENT_ACK_REC=0x40,
    SLA_R_SENT_NACK_REC=0x48,
    DAT_REC_ACK_SENT=0x50,
    DATA_REC_NACK_SENT=0x58,
} I2CStates_E;


#endif  // I2CSTATES_H

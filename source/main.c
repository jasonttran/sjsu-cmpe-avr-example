#include <stdint.h>

#include "FreeRTOS.h"
#include "task.h"

#include "i2c.h"


static void i2c_sample_task(void *param);


int main(void) 
{
    xTaskCreate(i2c_sample_task, "i2c_sample_task", 512U, NULL, PRIORITY_HIGH, NULL);
    vTaskStartScheduler();  // Returns only if insufficient RAM
    return -1;  // System should never return
}


static void i2c_sample_task(void *param)
{
    uint8_t data_buff[8U] = {0U};

    I2CMHandle_S i2c_master_handle = {
        .sla_addr=0x18,
        .reg_addr=0x00,
        .data_buff=data_buff,
        .data_buff_size=8U,
        .count=2U,
    };

    I2C_master_init();

    while (1 == 1) {
        I2C_master_read(&i2c_master_handle);
        
        while (!(I2C_is_master_trans_done())) {
            vTaskDelay(1U);
        }

        vTaskDelay(1000U);
    }
}

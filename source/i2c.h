#ifndef I2C_H
#define I2C_H

#include <stdbool.h>
#include <stdint.h>


// I2C Master Handle
typedef struct {
    uint8_t sla_addr;  // Target slave address
    uint8_t reg_addr;  // Target slave start register address
    uint8_t *data_buff;  // Pointer to a data buffer 
    uint8_t data_buff_size;  // Data buffer size
    uint8_t count;  // The desired number of bytes to transmit/receive
    bool done;  // Flag that indicates if the I2C transaction, for this I2C master handle, is done
} I2CMHandle_S;


void I2C_master_init(void);
uint8_t I2C_master_read(I2CMHandle_S *mhandle);
uint8_t I2C_master_write(I2CMHandle_S *mhandle);
bool I2C_is_master_trans_done(void);

#endif  // I2C_H
